-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 28, 2020 at 05:18 PM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laravelapi`
--

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `queue` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint(3) UNSIGNED NOT NULL,
  `reserved_at` int(10) UNSIGNED DEFAULT NULL,
  `available_at` int(10) UNSIGNED NOT NULL,
  `created_at` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `jobs`
--

INSERT INTO `jobs` (`id`, `queue`, `payload`, `attempts`, `reserved_at`, `available_at`, `created_at`) VALUES
(4, 'default', '{\"uuid\":\"95944d16-9cae-4fc5-9168-842ba5a8f1b7\",\"displayName\":\"App\\\\Notifications\\\\NewPostNotify\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"backoff\":\"\",\"timeout\":null,\"retryUntil\":null,\"data\":{\"commandName\":\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\",\"command\":\"O:48:\\\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\\\":14:{s:11:\\\"notifiables\\\";O:29:\\\"Illuminate\\\\Support\\\\Collection\\\":1:{s:8:\\\"\\u0000*\\u0000items\\\";a:1:{i:0;O:44:\\\"Illuminate\\\\Notifications\\\\AnonymousNotifiable\\\":1:{s:6:\\\"routes\\\";a:1:{s:4:\\\"mail\\\";s:18:\\\"harshv@zignuts.com\\\";}}}}s:12:\\\"notification\\\";O:31:\\\"App\\\\Notifications\\\\NewPostNotify\\\":10:{s:2:\\\"id\\\";s:36:\\\"1618ce2d-6c03-48db-8b95-3ee85012eb7d\\\";s:6:\\\"locale\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}s:8:\\\"channels\\\";a:1:{i:0;s:4:\\\"mail\\\";}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:19:\\\"chainCatchCallbacks\\\";N;s:5:\\\"delay\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1606486074, 1606486074);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(4, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(5, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(6, '2016_06_01_000004_create_oauth_clients_table', 1),
(7, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(8, '2019_08_19_000000_create_failed_jobs_table', 1),
(9, '2020_11_27_085819_create_posts_table', 2),
(10, '2020_11_27_132453_create_jobs_table', 3);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('066b9ad3c3031b163b1864beb5890fceb37d0c0f3229e1850db933224e87d64573ddc7ffd3cddcd4', 7, 1, 'LaravelAuthApp', '[]', 0, '2020-11-27 08:37:54', '2020-11-27 08:37:54', '2021-11-27 14:07:54'),
('07e1d9630b54c992a1c5d1aea99e8425065205cd7584e41783b9e88492bcaf0c89b4998ae56d321a', 1, 1, 'LaravelAuthApp', '[]', 0, '2020-11-27 05:09:59', '2020-11-27 05:09:59', '2021-11-27 10:39:59'),
('17104bc56186a676222bef511b4ca75ecaaddbd2b9adfd4a98efda595c7dc618c75b9697eeaad833', 1, 1, 'LaravelAuthApp', '[]', 0, '2020-11-27 05:56:23', '2020-11-27 05:56:23', '2021-11-27 11:26:23'),
('2ed17e867668cef442c574c3054c43eea02cebe491766ec3d1976c91b72adfdebfac97b165fc342b', 3, 1, 'LaravelAuthApp', '[]', 0, '2020-11-27 05:26:06', '2020-11-27 05:26:06', '2021-11-27 10:56:06'),
('51f9169a35fbc01288fd5d1f604cee55616f5b1c4d0a715ebd1b0da9288fbfe5ed436d836cc66820', 6, 1, 'LaravelAuthApp', '[]', 0, '2020-11-27 08:25:18', '2020-11-27 08:25:18', '2021-11-27 13:55:18'),
('65c1da2e17a87c5ecb2ecb4ae96c4b219379c7bfbdcc817a30b234e83d6a5795e8aef2d81d480ef3', 4, 1, 'LaravelAuthApp', '[]', 0, '2020-11-27 08:04:57', '2020-11-27 08:04:57', '2021-11-27 13:34:57'),
('6a52fa5635daa76203527d7ea776ba2f895659db60e92d537877c8d78c7c0e6f0be47731f2c60dd1', 5, 1, 'LaravelAuthApp', '[]', 0, '2020-11-27 08:13:53', '2020-11-27 08:13:53', '2021-11-27 13:43:53'),
('7f7eb8dc88c1468a0429bdd0461ad3431fb567f34f21680a57346cfe6ed6b2c9e50b0c15dbfd5360', 1, 1, 'LaravelAuthApp', '[]', 0, '2020-11-27 05:11:06', '2020-11-27 05:11:06', '2021-11-27 10:41:06'),
('852a7b2be4482e10ad8e2bc6be2a2cc9f963fc6e1388eff094c543e6f1e61f06c453a25c648d6519', 3, 1, 'LaravelAuthApp', '[]', 0, '2020-11-27 05:27:04', '2020-11-27 05:27:04', '2021-11-27 10:57:04'),
('aac265707b05dbcc20b7c9f2392eaf682df8a23a2daa91ec5e8a530be85cace7771e9586a42fb772', 1, 1, 'LaravelAuthApp', '[]', 0, '2020-11-27 07:31:52', '2020-11-27 07:31:52', '2021-11-27 13:01:52'),
('d1785e537060e5b191a45dac535249323f03f7afbc094599de00f193826c3185c73d5eafc10fb27f', 2, 1, 'LaravelAuthApp', '[]', 0, '2020-11-27 05:05:39', '2020-11-27 05:05:39', '2021-11-27 10:35:39'),
('d2cfe04eccf4502aa096bc8cc8dfaa28c9744af2c310028db860904e72b00f7ae5c462357177f365', 3, 1, 'LaravelAuthApp', '[]', 0, '2020-11-27 07:34:01', '2020-11-27 07:34:01', '2021-11-27 13:04:01'),
('da7d304a243fe31653f7a0c41bdb39f87e9e98f499635c57f3a0ad79502a74b2e0aa00a0c086650a', 1, 1, 'LaravelAuthApp', '[]', 0, '2020-11-27 05:44:28', '2020-11-27 05:44:28', '2021-11-27 11:14:28'),
('ead0df616bfd2ae7b8149b8185fcd33403ba0499791fbbe92da83fca3923136acd500563b68bef63', 3, 1, 'LaravelAuthApp', '[]', 0, '2020-11-27 07:34:11', '2020-11-27 07:34:11', '2021-11-27 13:04:11'),
('f53efd79197cb226ce65c296a9c5f03649ee15bb45e5604765dd3edb4cc71bf9caf443ad1f7a8093', 1, 1, 'LaravelAuthApp', '[]', 0, '2020-11-27 03:59:17', '2020-11-27 03:59:17', '2021-11-27 09:29:17'),
('fbce0e4f7c6ea4501aecff57550897c61141d422989a046321722863bb54ec8e5a6a903fec44340e', 1, 1, 'LaravelAuthApp', '[]', 0, '2020-11-27 05:08:40', '2020-11-27 05:08:40', '2021-11-27 10:38:40');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `provider`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', 'tq11Y7GzNSM2xMN7eAWKT84tJZBJH2YDDen8mj71', NULL, 'http://localhost', 1, 0, 0, '2020-11-27 03:22:08', '2020-11-27 03:22:08'),
(2, NULL, 'Laravel Password Grant Client', 'LsX8MC1Q84Or8KpIWqttnx5tpQavszYqenrfivX3', 'users', 'http://localhost', 0, 1, 0, '2020-11-27 03:22:08', '2020-11-27 03:22:08');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2020-11-27 03:22:08', '2020-11-27 03:22:08');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `user_id`, `title`, `description`, `created_at`, `updated_at`) VALUES
(1, 2, 'john Deo', 'This is testing', '2020-11-27 05:18:47', '2020-11-27 05:18:47'),
(2, 3, 'Demo Tester', 'This is final', '2020-11-27 05:31:52', '2020-11-27 05:31:52'),
(4, 1, 'Demo final2', 'This is not final2', '2020-11-27 05:33:21', '2020-11-27 05:33:21'),
(5, 2, 'Demo final3', 'This is not final3', '2020-11-27 05:34:28', '2020-11-27 05:34:28'),
(6, 1, 'Demo final4', 'This is not final4', '2020-11-27 05:35:45', '2020-11-27 05:35:45'),
(7, 3, 'Demo fina3', 'This is not fin3', '2020-11-27 05:37:29', '2020-11-27 05:37:29'),
(8, 1, 'harshvegad33@gmail.com', 'admin123', '2020-11-27 05:57:25', '2020-11-27 05:57:25'),
(9, 1, 'harshvegad33@gmail.com2', 'admin1232', '2020-11-27 05:58:20', '2020-11-27 05:58:20'),
(10, 2, 'harshvegad33@gmail.com2', 'admin1232', '2020-11-27 05:58:58', '2020-11-27 05:58:58'),
(11, 3, 'harshvegad33@gmail.com22', 'admin12322', '2020-11-27 06:01:14', '2020-11-27 06:01:14'),
(15, 3, 'harshvegad33@gmail.com22', 'admin123222', '2020-11-27 06:02:16', '2020-11-27 06:02:16'),
(16, 2, 'harshvegad33@gmail.co', 'admin', '2020-11-27 06:04:45', '2020-11-27 06:04:45'),
(17, 1, 'harshvegad33@gmail.co1', 'admin1', '2020-11-27 06:05:26', '2020-11-27 06:05:26'),
(18, 1, 'harshvegad33@gmail.co1', 'admin1', '2020-11-27 06:09:53', '2020-11-27 06:09:53'),
(19, 1, 'harshvegad33@gmail.co1', 'admin1', '2020-11-27 06:10:40', '2020-11-27 06:10:40'),
(20, 2, 'harshvegad33@gmail.co1', 'admin1', '2020-11-27 06:11:19', '2020-11-27 06:11:19');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'harsh', 'harshvegad33@gmail.com', NULL, '$2y$10$xxmvD8xGInTBvK/83.6d7OZapgRNHxC0UP8mQyP1XlbP141X5piHm', NULL, '2020-11-27 03:59:17', '2020-11-27 03:59:17'),
(2, 'Johm', 'john3@gmail.com', NULL, '$2y$10$cCnIaFNQPG.8Vx/Obtg.8Od1KXJclQOQOE0FFPbBaijgA1yDjc34K', NULL, '2020-11-27 05:05:37', '2020-11-27 05:05:37'),
(3, 'demo', 'demo33@gmail.com', NULL, '$2y$10$0T8/aftJZDJvyYgsYeaKWOnf2MAhtQztMJHP7W4LIMo5putYPglYy', NULL, '2020-11-27 05:26:06', '2020-11-27 05:26:06'),
(7, 'harsh demo', 'harshv@zignuts.com', NULL, '$2y$10$xKIPDeHVfUuY5wXT.eXRUuEeIcEEQmC.AfhM07YrObHKyVEiVGF7i', NULL, '2020-11-27 08:37:54', '2020-11-27 08:37:54');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jobs_queue_index` (`queue`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_auth_codes_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `posts_user_id_foreign` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `posts`
--
ALTER TABLE `posts`
  ADD CONSTRAINT `posts_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
